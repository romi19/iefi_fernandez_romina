#A partir del siguiente texto realizar un código Python que incluya el siguiente texto en una cadena y 
# realice los puntos abajo indicados, utilizar la biblioteca colorama para colorear o mejorar las salida por consola:

from colorama import init, Fore, Back, Style
init(True)

texto='GPT-3 es un sistema de escritura automática, basada en Machine Learning. En otras palabras: un algoritmo '\
    'capaz de escribir por sí mismo. O más bien, que ha aprendido a hacerlo. Sin pararnos en detalles demasiado técnicos, '\
    'GPT-3 es uno de los modelos con más parámetros para entrenarse. '\
    'Ha supuesto un cambio en la forma en la que se almacenan los datos, en cómo se entrena una Inteligencia Artificial y '\
    'de lo que es capaz de hacer. Y sobre todo, las sorprendentes aplicaciones que puede llegar a tener. Como es el caso de programar '\
    'código, O detectar fake news. '\
    'En efecto, GPT-3 está tan perfeccionado a día de hoy, que uno de sus “entrenamientos” es escribir noticias inventadas. '\
    'Y por supuesto, noticias falsas. El objetivo es sencillo: entrenarse a sí mismo para detectar cómo se escriben las noticias y '\
    'poder detectar cuáles son fake news. '\
    'En el caso de la programación, GPT-3 ha derivado en GitHub Copilot, “la famosa IA que sabe programar y que te puede dejar sin trabajo”. '\
    'Más allá de titulares llamativos, Copilot es una derivación de GPT-3 que en vez de ser entrenada '\
    'con lenguaje natural, ha sido entrenada a base de líneas de código. '
    
print(texto)
print()

#Definir una función largo_del_texto que reciba como parámetro el texto y devuelva un entero con la longitud en caracteres, 
# llamar la función desde el código principal, mostrar el texto y su longitud en cantidad de caracteres.
def  largo_del_texto(t):
    contador=0

    for x in t:
        contador+=1

    return contador


#Definir una función lista_con_palabras_1 que reciba como parámetro el texto y retorne una lista de palabras que incluya todos los
# caracteres, la cantidad de palabras, desde el programa principal llamar la función, guardar el resultado en nueva lista en lista1
# y mostrar por consola los resultados.

def lista_con_palabras_1 (texto):
    lista1=texto.split()
    palabras=len(lista1)
    
    return lista1 , palabras
    

#Definir una función lista_con_palabras_2 que reciba como parámetro una lista de palabras,
# encontrar el mayor y el menorelemento en cantidad de caracteres de los elementos y sus respectivas posiciones dentro de la lista,
# devolver los resultados, llamar la función, pasarle como parámetro lista1 y mostrar los resultados por la consola.

def lista_con_palabras_2 (lista1):
    menor=min(lista1, key=len)
    posmenor=lista1.index(menor)
    mayor=max(lista1, key=len)
    posmayor=lista1.index(mayor)
    
    return menor, posmenor, mayor, posmayor


#Definir una función contar_menores_mayores que reciba como parámetro la lista1, el menor, 
# el mayor, devuelva como resultado la cantidad de mayores, la cantidad de elementos menores, menores,
# llamar de la función desde el código principal y mostrar los resultados.

def contar_menores_mayores(lista1, menor,mayor):
    contador_menor=0
    contador_mayor=0
    for x in lista1:
        if x == menor:
            contador_menor += 1
        if x == mayor:
            contador_mayor += 1
    print('La palabra menor aparece {} veces'.format(contador_menor))
    print('La palabra mayor aparece {} veces'.format(contador_mayor))
    print()
    return


r=largo_del_texto(texto)
print('la cantidad de caracteres es: ',r)
print()

lista1=lista_con_palabras_1(texto)[0]
palabras=lista_con_palabras_1(texto)[1]
print( lista1 )
print()
print( 'la cantidad de palabras es: ', palabras)
print()

m=lista_con_palabras_2(lista1)
print('El elemento menor es:', '"' + m[0] + '"', 'en la posición:', m[1])
print('El elemento mayor es:', '"' + m[2] + '"', 'en la posición:', m[3])
print()

contar_menores_mayores(lista1, m[0], m[2])